from pandas_ods_reader import read_ods
from pathlib import Path
import pandas as pd
import common

tax_file = str(Path(__file__).parent.joinpath('../data/tax_data.json'))
tax = common.get_taxonomy_concepts_json(tax_file)
concepts = tax["data"]["concepts"]
tax_list = [x for x in concepts if x["type"] in ["skill"]]

AUB_moduler_file = pd.read_excel('../data/React_alla_AUB-moduler_segmenterade.xlsx',
                                 usecols=['modulens id', 'modulens namn', 'modulens beskrivning', 'utbildningsnamn'])
beskrivning_list = [row.lower() for row in AUB_moduler_file["modulens beskrivning"]]
utbildningsnamn_list = [row.lower() for row in AUB_moduler_file["utbildningsnamn"]]

ModulensBeskrivning = read_ods('../data/ModulensBeskrivning.ods',
                                columns=['Modulens', 'Concept Id', 'Skill Preferred Label', 'Distance'])
Utbildningsnamn = read_ods('../data/Utbildningsnamn.ods',
                            columns=['Modulens', 'Concept Id', 'Skill Preferred Label', 'Distance'])

def main():
      common.get_connected_concepts_ods_Modulens(tax_list, beskrivning_list, "../data/ModulensBeskrivning.ods")
      print("ModulensBeskrivning.ods Done!")

      common.get_connected_concepts_ods_Modulens(tax_list, utbildningsnamn_list, "../data/Utbildningsnamn.ods")
      print("Utbildningsnamn.ods Done!")

      ModulensBeskrivning = read_ods('../data/ModulensBeskrivning.ods',
                                columns=['Modulens', 'Concept Id', 'Skill Preferred Label', 'Distance'])
      Utbildningsnamn = read_ods('../data/Utbildningsnamn.ods',
                            columns=['Modulens', 'Concept Id', 'Skill Preferred Label', 'Distance'])
      
      common.render_table_result("../data/SkillsforAUB.ods", ModulensBeskrivning, Utbildningsnamn, AUB_moduler_file)
      print("SkillsforAUB.ods Done!")

if __name__ == '__main__':
    main()

