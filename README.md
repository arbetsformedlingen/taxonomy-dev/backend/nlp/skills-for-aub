# skills for AUB

This repository contains candidate skills for AUB to help Redaction Team to easily find out semantic connected
between Modulens and Taxonomy Skills. We used Sentence-BERT model to find semantic connected between Modulens and Taxonomy Skills.

## Set up environment

- Make sure python 3.11 is installed.
- Make sure pip is installed.
- Set up a venv (IntelliJ or similar environment):
  - File -> Project Structure -> SDKs -> + -> Add Python SDK... -> Virtual ENV Environment
- Install dependencies:
  `pip install - r requirements.txt`

## Expected result:

`SkillsforAUB.ods` is the expected result when you run `main.py`.
