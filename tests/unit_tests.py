import unittest
import os, sys
sys.path.insert(0, os.path.abspath("../skills_for_AUB"))
import common
import main

class MyTestCase(unittest.TestCase):
    def test_get_preferred_labels(self):
        tax_list = main.tax_list
        tax_labels = [common.get_preferred_labels(concept) for concept in tax_list]
        self.assertTrue(['Arbetsledarerfarenhet'] in tax_labels)

    def test_flatten(self):
        tax_list = main.tax_list
        tax_labels = [common.get_preferred_labels(concept) for concept in tax_list]
        tax_labels =[label.lower() for label in common.flatten(tax_labels)]
        self.assertTrue('arbetsledarerfarenhet' in tax_labels)

    def test_group_by_first(self):
        string = 'senaste programvara inom adobe photoshop.'
        group_by = common.group_by_first(main.ModulensBeskrivning.values)
        output_list = group_by['senaste programvara inom adobe photoshop.']
        self.assertEqual(string, output_list[0].any())

    def test_beskrivning_list(self):
        string = 'senaste programvara inom adobe photoshop.'
        beskrivning_list = main.beskrivning_list
        self.assertTrue(string in beskrivning_list)
        self.assertEqual(9804, len(beskrivning_list))

    def test_utbildningsnamn_list(self):
        string = 'kultur - adobe inriktning bild'
        utbildningsnamn_list = main.utbildningsnamn_list
        self.assertTrue(string in utbildningsnamn_list)
        self.assertEqual(9804, len(utbildningsnamn_list))

if __name__ == '__main__':
    unittest.main()
